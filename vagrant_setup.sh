#!/bin/bash

export DEBIAN_FRONTEND=noninteractive
export UCF_FORCE_CONFOLD=1

# Updates
sudo -E apt-get -y update
sudo -E apt-get -y upgrade

# install basic stuff
sudo -E apt-get -y install python3-pip gdb gdb-multiarch unzip libc6-dbg

# Install ARM/MIPS support - https://ownyourbits.com/2018/06/13/transparently-running-binaries-from-any-architecture-in-linux-with-qemu-and-binfmt_misc/
# sudo -E apt-get -y install gcc-arm-linux-gnueabihf
# sudo -E apt-get -y install qemu qemu-user qemu-user-static
# sudo -E apt-get -y install debian-keyring
# sudo -E apt-get -y install debian-archive-keyring
# sudo -E apt-get -y install libc6-armel-cross libc6-dev-armel-cross binutils-arm-linux-gnueabi libncurses5-dev
# sudo -E apt-get -y install libc6-mipsel-cross
# sudo -E apt-get -y install libc6-arm64-cross libc6-armhf-cross libc6-armel-armhf-cross libc6-armel-cross libc6-armhf-armel-cross
# echo "export QEMU_LD_PREFIX=/usr/arm-linux-gnueabihf" >> /home/vagrant/.zshrc

# Install Pwntools
sudo -E apt-get -y install python2.7 python-pip python-dev git libssl-dev libffi-dev build-essential
sudo -E -H pip install --upgrade pip
sudo -E -H pip install --upgrade pwntools

sudo -E -H python3 -m pip install --upgrade pip
sudo -E -H python3 -m pip install --upgrade git+https://github.com/Gallopsled/pwntools.git@beta

cd ~
mkdir tools
cd tools

# Install capstone
sudo -E apt-get -y install libcapstone-dev

# Install pwndbg
git clone https://github.com/zachriggle/pwndbg
cd pwndbg
./setup.sh

# Install pwndbg for root
sudo -E su << HERE
git clone https://github.com/zachriggle/pwndbg
cd pwndbg
./setup.sh
HERE

# pycparser for pwndbg
sudo -E -H pip3 install pycparser # Use pip3 for Python3

# Install binwalk
cd tools
git clone https://github.com/devttys0/binwalk
cd binwalk
sudo -E python setup.py install

# oh-my-zsh
sudo -E apt-get -y install zsh
echo vagrant | sh -c "$(wget https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"

# Install Angr
cd /home/vagrant/tools
sudo -E apt-get -y install python-dev libffi-dev build-essential virtualenvwrapper
sudo -E -H pip install virtualenv
virtualenv angr
source angr/bin/activate
pip install angr ipython --upgrade

# Enable 32bit binaries on 64bit host
sudo -E dpkg --add-architecture i386
sudo -E apt-get -y update
sudo -E apt-get -y upgrade
sudo -E apt-get -y install libc6:i386 libc6-dbg:i386 libncurses5:i386 libstdc++6:i386

# Install one_gadget
sudo -E apt-get -y install ruby-full
sudo -E gem install one_gadget

# Install ida debug server as a service

sudo -E bash -c 'cat > /etc/systemd/system/dbgsrv64.service << EOF
[Unit]
Description=linux_server64 service.
After=network.target

[Service]
Type=simple
Restart=always
RestartSec=1
User=root
ExecStart=/home/vagrant/dbgsrv/linux_server64

[Install]
WantedBy=multi-user.target
EOF'

sudo -E systemctl enable dbgsrv64
sudo -E service dbgsrv64 start

sudo -E bash -c 'cat > /etc/systemd/system/dbgsrv.service << EOF
[Unit]
Description=linux_server service.
After=network.target

[Service]
Type=simple
Restart=always
RestartSec=1
User=root
ExecStart=/home/vagrant/dbgsrv/linux_server -p 23947

[Install]
WantedBy=multi-user.target
EOF'

sudo -E systemctl enable dbgsrv
sudo -E service dbgsrv start

# Heap Viewer plugin
cd ~/tools
git clone https://github.com/danigargu/heap-viewer.git
cd heap-viewer/utils/
#python2 get_config.py > ~/heap_viewer_config.txt