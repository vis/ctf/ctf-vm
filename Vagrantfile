# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.

require_relative "config"
conf = get_config()

def config_provider(node)
    node.vm.provider "virtualbox" do |provider|
        yield provider
    end
    node.vm.provider "vmware_free" do |provider|
        yield provider
    end
    # Uncomment for WSL2:
#    node.vm.provider "virtualbox" do |vb|
#        vb.customize [ "modifyvm", :id, "--uartmode1", "disconnected" ]
#    end
end

def run_ansible(config, playbook, opts)
    conf = get_config()
    # Uncomment for WSL2:
#    config.ssh.host = ENV["CTF_VM_IP"]
    config.vm.provision "ansible" do |ansible|
        ansible.playbook = "playbooks/#{playbook}-play.yaml"
        ansible.become = true
        ansible.verbose = "-v"
        ansible.extra_vars = {
            ansible_python_interpreter: "/usr/bin/python3",
            ssh_key: conf[:ssh_key],
            shell: conf[:shell],
            common_archs: conf[:common_archs],
            qemu_archs: conf[:qemu][:archs],
            hostname: opts[:name],
            ip_addr: opts[:ip]
        }
        # ansible.raw_arguments = ["--ask-become-pass"]
    end
end

Vagrant.configure("2") do |config|
    # The most common configuration options are documented and commented below.
    # For a complete reference, please see the online documentation at
    # https://docs.vagrantup.com.

    # Every Vagrant development environment requires a box. You can search for
    # boxes at https://vagrantcloud.com/search.
    config.ssh.username = "vagrant"

    # Shared folders
    conf[:shared_folders].each do |host, guest|
        config.vm.synced_folder(host, guest)
    end

    conf[:libc_versions].each do |libc, opts|
        config.vm.define "#{libc}" do |node|
            # Provisioning
            run_ansible(node, "initial", opts)
            run_ansible(node, "pwntools", opts)
            if conf[:qemu][:enabled] then
                run_ansible(node, "qemu", opts)
            end
            if conf[:gdb_ext] == :pwndbg then
                run_ansible(node, "pwndbg", opts)
            elsif conf[:gdb_ext] == :gef then
                run_ansible(node, "gef", opts)
            end
            run_ansible(node, "tools", opts)
            run_ansible(node, "ropium", opts)
            if conf[:ida][:enabled] then
                run_ansible(node, "ida", opts)
            end
            if conf[:remenissions][:enabled] then
              run_ansible(node, "remenissions", opts)
            end

            node.vm.box = opts[:box]
            node.vm.define opts[:name]
            
            # Networks
            node.vm.network :private_network, ip: opts[:ip]
            # disable default forwarding so you can have two or more boxes up at the same time
            node.vm.network :forwarded_port, id: "ssh", guest: 22, host: 2200 + libc, auto_correct: true
            # https://kb.vmware.com/s/article/52683?lang=en_US&queryTerm=ubuntu+boot+hang
            # Provider Specific
            config_provider(node) do |provider|
                provider.name = opts[:name]
                provider.memory = conf[:num_mem]
                provider.cpus = conf[:num_cores]
            end
        end
    end
end
